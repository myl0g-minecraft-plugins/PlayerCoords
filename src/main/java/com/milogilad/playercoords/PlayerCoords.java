package com.milogilad.playercoords;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public final class PlayerCoords extends JavaPlugin {

	@Override
	public void onEnable() {
		getLogger().info("onEnable invoked! - playercoords");
	}
	
	@Override
	public void onDisable() {
		getLogger().info("onDisable invoked! - playercoords");
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("pcoords")) {

			if (!sender.hasPermission("playercoords.pcoords")) {
				sender.sendMessage("You're not permitted to use that command!");
				return false;
			}
			
			String result = getPlayerCoords(args[0]);
			
			if (result != null) {
			    sender.sendMessage(result);
				return true;
			} else {
				sender.sendMessage("Player is not online or does not exist!");
				return false;
			}
			
		}

		return false;
	}
	
	private String getPlayerCoords(String username) {
		Player p = null;
		
		getLogger().info("Looking for " + username);
		
		for (Player o : Bukkit.getServer().getOnlinePlayers()) {
			getLogger().info(o.getName());
			if (o.getName().equals(username)) {
				getLogger().info("Match!");
				p = o;
				break;
			}
		}
		
		if (p == null) {
			return null;
		}
		
		Location l = p.getLocation();
		String s = Double.toString(l.getX()) + " " + Double.toString(l.getY()) + " " + Double.toString(l.getZ());
		
		return s;
	}
}
