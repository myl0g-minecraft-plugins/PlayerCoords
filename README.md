# PlayerCoords

A Minecraft bukkit plugin adding a command to obtain the coordinates of a given player.

## Usage

`/pcoords [username]`

## Permissions

playercoords.pcoords (op-only by default)
